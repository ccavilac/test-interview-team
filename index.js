/***
 * Implementation
 * 1. create the functionality
 */

// variable for accumulate the value
let accum = 0;

// exposed function for sumatory
const sum = (value) => {
    // cases values is not a function
    if(typeof(value) !== 'function') {
        // accumulate de value
        accum += value;
        // return the function to be evaluated
        return sum;
    // case value is a function
    } else {
        // auxiliar for the accumulation
        const result = accum;
        // reset accumulation
        accum = 0;
        // return the execution of the function passed
        return value(result);
    }
}

// exposed function for extract values
const extractValuesForKey = (object, searhedKey) => {
    // levels initialization
    let levels = 1;
    // create a new map empty
    let map = new Map();
    // root
    let paths = [];
    // function for recursion
    const evaluateLevels = (obj, key) => {
        // get instance from the current object
        const currentObject = Object.keys(obj);
        // extract keys from current object
        currentObject.forEach((enterKey, index) => {
            // case key conincidence
            if(enterKey === key) {
                // validate if found element in root
                const newPath = paths.length > 0 ? paths.join('/') + '/' : '';
                // set in the map key and value
                map.set(newPath, levels);
                // level up
                levels += 1;
            }
            // case current key was an object
            if (typeof(obj[enterKey]) === 'object') {
                // save the key
                paths.push(enterKey);
                // call the function for evaluation
                evaluateLevels(obj[enterKey], key);
            }
            if(currentObject.length - 1 === index) {
                paths.pop();
            }
        });
    };

    // invoke function for recursion
    evaluateLevels(object, searhedKey);

    // return map
    return map;
}

// module exportation
module.exports = { sum, extractValuesForKey };
