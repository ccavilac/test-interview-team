/***
 * Test Driven Development Strategy
 * 1. start with test cases
 * 2. create the functionality
 */

// import file for test cases
const tasks = require('./index');

describe('tests for sum functionality', () => {

    it('test for creation ', () => {
        expect(tasks).toBeDefined();
    });

    it('test for empty value', () => {
        const logSpy = jest.spyOn(console, 'log');
        tasks.sum(result => console.log('-> ', result));
        expect(logSpy).toHaveBeenCalledWith('-> ', 0);
    });

    it('test for first value', () => {
        const logSpy = jest.spyOn(console, 'log');
        tasks.sum(1)(result => console.log('-> ', result))
        expect(logSpy).toHaveBeenCalledWith('-> ', 1);
    });

    it('test for second value', () => {
        const logSpy = jest.spyOn(console, 'log');
        tasks.sum(1)(2)(result => console.log('-> ', result));
        expect(logSpy).toHaveBeenCalledWith('-> ', 3);
    });

    it('test for third value', () => {
        const logSpy = jest.spyOn(console, 'log');
        tasks.sum(1)(2)(4)(result => console.log('-> ', result));
        expect(logSpy).toHaveBeenCalledWith('-> ', 7);
    });

    it('test for task 2', () => {
        const logSpy = jest.spyOn(console, 'log');
        tasks.sum(1)(2)(3)(4)(5)(6)(result => console.log('-> ', result));
        expect(logSpy).toHaveBeenCalledWith('-> ', 21);
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });
    
});

const mockObj = {
  _id: "621da3f3ae1fe1e115b4bb64",
  index: 0,
  guid: "74ea2ba2-940c-4bb2-b70b-36e473d3ab9c",
  tags: {
    index: 0,
    cols: {
      index: 0,
      guid: "f4add871-b04f-45c4-9d45-e1c82f4e36c7",
      tags: {
        index: 0,
        guid_0: "c7148f4f-32df-4289-aceb-702f5ad92a94",
        cols: {
          index: 0,
          guid_1: "34f7b824-23d8-4a82-803b-c2d1eae30dd2",
        },
      },
    },
    team: {
        incredible: {
            ridiculously: {
                long: {
                    nested: {
                        object: {
                            object: {
                                hey: {
                                    finally: 'value'
                                }
                            }
                        }
                    }
                }
            },
            unbeliebed: {
                long: {
                    nested: {
                        object: {
                            object: {
                                hey: {
                                    finally: 'value'
                                }
                            }
                        }
                    }
                }
            }

        }
    }
  },
};

describe('tests for extract values in a map', () => {

    it('test for function returned cero coincidences', () => {
        const maped = tasks.extractValuesForKey(mockObj, 'theKey');
        expect(maped.size).toBe(0);
    });

    it('test for function returned a coincidence', () => {
        const maped = tasks.extractValuesForKey(mockObj, '_id');
        expect(maped.size).toBe(1);
    });

    it('test for function returned a coincidence and check value', () => {
        const maped = tasks.extractValuesForKey(mockObj, '_id');
        expect(maped.get('')).toBe(1);
    });

    it('test for function returned two coincidences and check value', () => {
        const maped = tasks.extractValuesForKey(mockObj, 'guid');
        expect(maped.get('tags/cols/')).toBe(2);
    });

    it('test for function returned two coincidences with key "tag" and check value', () => {
        const maped = tasks.extractValuesForKey(mockObj, 'tags');
        expect(maped.get('tags/cols/')).toBe(2);
    });

    it('test for function returned two coincidences with diferentes levels key "guid_1" and check value', () => {
        const maped = tasks.extractValuesForKey(mockObj, 'guid_1');
        expect(maped.get('tags/cols/tags/cols/')).toBe(1);
    });

    it('test for function returned two coincidences with diferentes levels key "finally" and check value', () => {
        const maped = tasks.extractValuesForKey(mockObj, 'finally');
        expect(maped.get('tags/team/incredible/ridiculously/long/nested/object/object/hey/')).toBe(1);
    });

    it('test for function returned two coincidences with diferentes path container key "finally" and check value', () => {
        const maped = tasks.extractValuesForKey(mockObj, 'finally');
        expect(maped.get('tags/team/incredible/unbeliebed/long/nested/object/object/hey/')).toBe(2);
    });

});
